package lession01;

public class NhanVien extends CanBo {
    private String congViec;

    public NhanVien(String congViec) {
        this.congViec = congViec;
    }

    public NhanVien(String ten, int tuoi, String gioiTinh, String diaChi, String congViec) {
        super(ten, tuoi, gioiTinh, diaChi);
        this.congViec = congViec;
    }

    public String getCongViec() {
        return congViec;
    }

    public void setCongViec(String congViec) {
        this.congViec = congViec;
    }

    public String toString() {
        return "\nNhân viên: "+  "\nBậc: "+ this.getCongViec()+ "\nTên: " + this.getTen() + "\nTuoi: " + this.getTuoi() + "\nGioi Tinh: " + this.getGioiTinh() + "\nĐịa chỉ: " + this.getDiaChi();
    }
}
