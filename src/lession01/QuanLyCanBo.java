package lession01;

import java.util.ArrayList;
import java.util.List;

public class QuanLyCanBo {
    private List<CanBo> canBo;

    public QuanLyCanBo() {
        canBo = new ArrayList<>();
    }

    public void addCanBo(CanBo canBo) {
        this.canBo.add(canBo);
    }

    public void listAll() {
        System.out.println(canBo.toString());
    }

    public void timKiem(String ten) {
        for (CanBo bo : canBo) {
            if (bo.getTen().toLowerCase().contains(ten)) {
                System.out.println(bo.toString());
            }
        }
    }

}
