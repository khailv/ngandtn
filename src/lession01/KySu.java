package lession01;

public class KySu extends CanBo {
    private String nghanhDaoTao;

    public KySu() {

    }

    public KySu(String ten, int tuoi, String gioiTinh, String diaChi, String nghanhDaoTao) {
        super(ten, tuoi, gioiTinh, diaChi);
        this.nghanhDaoTao = nghanhDaoTao;
    }

    public String getNghanhDaoTao() {
        return nghanhDaoTao;
    }

    public void setNghanhDaoTao(String nghanhDaoTao) {
        this.nghanhDaoTao = nghanhDaoTao;
    }

    public String toString() {
        return "\nKỹ sư: "+  "\nBậc: "+ this.getNghanhDaoTao()+ "\nTên: " + this.getTen() + "\nTuoi: " + this.getTuoi() + "\nGioi Tinh: " + this.getGioiTinh() + "\nĐịa chỉ: " + this.getDiaChi();
    }
}
