package lession01;

public class CongNhan extends CanBo {
    private int bac;

    public CongNhan() {

    }

    public CongNhan(String ten, int tuoi, String gioiTinh, String diaChi, int bac) {
        super(ten, tuoi, gioiTinh, diaChi);
        this.bac = bac;
    }

    public int getBac() {
        return bac;
    }

    public void setBac(int bac) {
        this.bac = bac;
    }

    public String toString() {
        return "\nCông nhân: "+  "\nBậc: "+ this.getBac()+ "\nTên: " + this.getTen() + "\nTuoi: " + this.getTuoi() + "\nGioi Tinh: " + this.getGioiTinh() + "\nĐịa chỉ: " + this.getDiaChi();
    }
}
