package lession01;

public class Main {
    public static void main(String[] args) {
        CanBo canBo1 = new CongNhan("Khai Le", 25, "Nam", "96 Ha Thi", 6);
        CanBo canBo2 = new CongNhan("KhaiLV", 28, "Nam", "TMT2", 9);
        CanBo canBo3 = new KySu("Nguyen Van", 30, "Nam", "Tan Thuan", "Cơ khí");

        QuanLyCanBo quanLyCanBo = new QuanLyCanBo();

        quanLyCanBo.addCanBo(canBo1);
        quanLyCanBo.addCanBo(canBo2);
        quanLyCanBo.addCanBo(canBo3);
        quanLyCanBo.listAll();
        quanLyCanBo.timKiem("khai");
    }
}
