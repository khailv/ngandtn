package learning.designpattern.factory;

public class ComputerClient {
    public static void main(String[] args) {
        Computer PC = ComputerFactory.createComputer(ComputerType.PC, "4GB", "256GB", "2.3Hz");
        Computer server = ComputerFactory.createComputer(ComputerType.SERVER, "32GB", "2TB", "1.8Hz");

        Computer superComputer = ComputerFactory.createComputer(ComputerType.SUPERCOMPUTER, "4GB", "256GB", "2.3Hz");

    }
}
