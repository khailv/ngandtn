package learning.designpattern.factory;

public interface Computer {
    public String getRAM();
    public String getHDD();
    public String getCPU();
}
