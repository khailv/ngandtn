package learning.designpattern.factory;

public class ComputerFactory {
    public static Computer createComputer(ComputerType type, String RAM, String HDD, String CPU) {
        Computer computer = null;
        switch (type) {
            case PC:
                computer = new PC(RAM, HDD, CPU);
                break;
            case SERVER:
                computer = new Server(RAM, HDD, CPU);
                break;
            case SUPERCOMPUTER:
                computer = new SuperComputer();


        }
        return computer;
    }
}
