package learning.designpattern.factory;

public enum ComputerType {
    PC, SERVER, SUPERCOMPUTER
}
