package learning.designpattern.lession07;

public class Main {
    public static void main(String[] args) {
        Car car, luxuryCar;
        car = new Car("A");
        luxuryCar = new LuxuryCar("B");
        car.showDescription();
        luxuryCar.showDescription();

        if (car instanceof LuxuryCar) {
            System.out.println("log 1");
            ((LuxuryCar) car).showAll();
        }
        if (luxuryCar instanceof LuxuryCar) {
            System.out.println("log 2");
            ((LuxuryCar) luxuryCar).showAll();
        }

        //design pattern
    }
}
