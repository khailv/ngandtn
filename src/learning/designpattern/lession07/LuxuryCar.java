package learning.designpattern.lession07;

public class LuxuryCar extends Car {
    public LuxuryCar(String name) {
        super(name);
    }

    @Override
    public void showDescription() {
        System.out.println("Luxyry Car: " + name);
    }

    public void showAll() {
        System.out.println("Show all");
    }
}
