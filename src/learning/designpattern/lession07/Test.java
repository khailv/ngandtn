package learning.designpattern.lession07;

public class Test {
    public static void main(String[] args) {
        CarMgm car1 = CarMgm.getInstance();
        car1.addCar( new Car("Toyota"));

        CarMgm.getInstance().addCar(new LuxuryCar("Hyundai"));
        CarMgm.getInstance().showAll();
    }
}
