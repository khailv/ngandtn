package learning.designpattern.lession07;

import java.util.ArrayList;
import java.util.List;

public class CarMgm {
    private static CarMgm instance;
    List<Car> cars = new ArrayList<>();

    private CarMgm() {

    }

    public static CarMgm getInstance() {
        if (instance == null) {
            synchronized (CarMgm.class) {
                if (instance == null) {
                    instance = new CarMgm();
                }
            }
        }

        return instance;
    }

    public void addCar(Car car) {
        cars.add(car);
    }

    public void showAll() {
        for (Car car : cars) {
            car.showDescription();
        }
    }
}
